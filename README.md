# nbuthesis-EECS-latex

#### 介绍
宁波大学信息科学与工程学院本科生毕设latex模板


#### 使用说明

1.  参考example.tex进行配置使用
2.  使用xelatex编译，编译顺序：xelatex -> biber -> xelatex -> xelatex

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 其他

主仓库：https://gitee.com/luheqiu/nbuthesis-eecs-latex

已移植至overleaf在线latex编辑器，可在线体验
https://www.overleaf.com/latex/templates/nbuthesis-eecs-latex-zhu-bo-da-xue-bi-ye-lun-wen-mo-ban/srwxmysmzsyk

